<?php

/**
 * @file
 * Definition of Drupal\views_bulk_operations\Plugin\Type\OperationTypeManager.
 */

namespace Drupal\views_bulk_operations\Plugin\Type;

use Drupal\Component\Plugin\PluginManagerBase;
use Drupal\Component\Plugin\Factory\DefaultFactory;
use Drupal\Core\Plugin\Discovery\AlterDecorator;
use Drupal\Core\Plugin\Discovery\AnnotatedClassDiscovery;
use Drupal\Core\Plugin\Discovery\CacheDecorator;

/**
 * Plugin manager for operation types.
 */
class OperationTypeManager extends PluginManagerBase {

  /**
   * The cache bin used for plugin definitions.
   *
   * @var string
   */
  protected $cacheBin = 'cache';

  /**
   * The cache id used for plugin definitions.
   *
   * @var string
   */
  protected $cacheID = 'views_bulk_operations:operation_types';

  /**
   * Constructs an OperationTypeManager object.
   */
  public function __construct() {
    $this->baseDiscovery = new AlterDecorator(new AnnotatedClassDiscovery('views_bulk_operations', 'operation_type'), 'views_bulk_operations_plugins_operation_type');
    $this->discovery = new CacheDecorator($this->baseDiscovery, $this->cacheID, $this->cacheBin);
    $this->factory = new DefaultFactory($this);
  }

}
