<?php

/**
 * @file
 * Definition of Drupal\views_bulk_operations\ViewsBulkOperationsBundle.
 */

namespace Drupal\views_bulk_operations;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Drupal\views\ViewExecutable;

/**
 * views_bulk_operations dependency injection container.
 */
class ViewsBulkOperationsBundle extends Bundle {

  /**
   * Overrides Symfony\Component\HttpKernel\Bundle\Bundle::build().
   */
  public function build(ContainerBuilder $container) {
    $container->register('plugin.manager.views_bulk_operations.operation_type', 'Drupal\views_bulk_operations\Plugin\Type\OperationTypeManager');
  }

}
